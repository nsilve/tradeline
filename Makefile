NAME=td
VERSION=1.0.0

build:
	docker build -t $(NAME):$(VERSION) .

run:
	docker run --rm -v /var/run/docker.sock:/var/run/docker.sock --network host $(NAME):$(VERSION)

clean:
	docker rmi $(NAME):$(VERSION)
