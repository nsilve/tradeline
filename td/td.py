import argparse
import os
import sys

from td.docker.log import log
from td.docker.build import build
from td.docker.run import run
from td.docker.stop import stop
from td.docker.monitor import monitor
from td.docker.check import check
from td.docker.logf import logf


def main(args):
    """Main function
    
    Arguments:
        args {List} -- List of line arguments passed
    """

    action_func = {
        'build': 'build',
        'run': 'run',
        'check': 'check',
        'monitor': 'monitor',
        'log': 'log',
        'logf': 'logf',
        'stop': 'stop',
    }

    parser = argparse.ArgumentParser(
        prog='td',
        description="Tradeline Docker CLI")

    parser.add_argument(
        'action',
        choices=action_func.keys(),
        help='available actions'
    )

    parser.add_argument(
        '-v',
        '--debug',
        action="store_true",
        help='show verbose output'
    )

    args = parser.parse_args(args[1:])

    func = getattr(sys.modules[__name__], action_func[args.action])

    if args.action == 'build':
        func(os.getcwd() + '/webapp/Dockerfile', 'webapp:1.0.0', debug=args.debug)
    elif args.action == 'check':
        func('5000/tcp')
    elif args.action == 'run':
        func('webapp:1.0.0', '5000/tcp', debug=args.debug)
    else:
        func(debug=args.debug)
