import td


def log(debug=False):
    """Prints the logs of all running containers sorted by time

    Keyword Arguments:
        debug {bool} -- debug switch (default: {False})
    """

    lines = []

    for c in td.docker.common.get_running_containers():
        for line in c.logs(timestamps=True).decode().splitlines():
            lines.append(td.docker.common.format_container_log_line(c.name, line))

    lines.sort()

    for line in lines:
        print(line)
