import td
import td.logger.logger

logger = td.logger.logger.get_logger(__name__)


def monitor(debug=False):
    """Prints the stats of the running containers
    
    Keyword Arguments:
        debug {bool} -- debug switch (default: {False})
    """

    print("{:<20} {:<8} {:<9} {:<13}".format('NAME', 'CPU (%)', 'MEM (MB)', 'NET I/O (kB)'))

    for c in td.docker.common.get_running_containers():

        cpu = 0.0
        stats = c.stats(stream=False)

        delta_total_usage = stats['cpu_stats']['cpu_usage']['total_usage'] - stats['precpu_stats']['cpu_usage']['total_usage']
        delta_system_usage = stats['cpu_stats']['system_cpu_usage'] - stats['precpu_stats']['system_cpu_usage']

        if delta_system_usage > 0.0:
            cpu = (delta_total_usage / delta_system_usage) * len(stats['cpu_stats']['cpu_usage']['percpu_usage']) * 100.0;

        print("{:<20} {:<8.2f} {:<9.2f} {:.2f}/{:.2f}".format(c.name, cpu, stats['memory_stats']['usage']/1024**2, stats['networks']['eth0']['rx_bytes']/1024, stats['networks']['eth0']['tx_bytes']/1024))



