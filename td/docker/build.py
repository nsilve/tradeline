import docker
import os
import td.logger.logger

logger = td.logger.logger.get_logger(__name__)


def build(dockerfile, image_tag, debug=False):
    """Builds docker image
    
    Arguments:
        dockerfile {str} -- DcokerFile path
        image_tag {str} -- Image name
    
    Keyword Arguments:
        debug {bool} -- debug switch (default: {False})
    """

    client = docker.from_env()

    dockerfile_path = os.path.dirname(dockerfile)
    dockerfile_name = os.path.basename(dockerfile)

    if not os.path.exists(dockerfile_path):
        logger.error('%s folder does not exist', dockerfile_path)
        return

    if not os.path.exists(dockerfile):
        logger.error('%s file does not exist', dockerfile)
        return

    try:
        image, lines = client.images.build(
            path=dockerfile_path,
            dockerfile=dockerfile_name,
            tag=image_tag
        )

        if debug:
            for line in lines:
                if 'stream' in line:
                    logger.debug(line['stream'].rstrip())

        logger.info('Image %s has been successfully built', image.tags[0])

    except Exception as e:
        logger.error(e.__class__.__name__ + ": " + str(e))


