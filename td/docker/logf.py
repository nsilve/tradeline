import td
import td.logger.logger
import threading

logger = td.logger.logger.get_logger(__name__)


def print_logs(name, generator):
    """Prints all log lines provided by a generator
    
    Arguments:
        name {str} -- Container name
        generator {generator} -- Generator providing the log lines
    """

    for i in generator:
        print(td.docker.common.format_container_log_line(name, i.decode()))


def logf(debug=False):
    """Prints the logs of all running containers with follow capability
    
    Keyword Arguments:
        debug {bool} -- debug switch (default: {False})
    """

    threads = []

    try:
        for c in td.docker.common.get_running_containers():
            log_generator = c.logs(timestamps=True, stream=True, follow=True)
            name_generator = (c.name, log_generator)
            thread = threading.Thread(target=print_logs, args=name_generator)
            threads.append(thread)
            thread.setDaemon(True)
            thread.start()

        for thread in threads:
            thread.join()

    except KeyboardInterrupt:
        pass
    except Exception as e:
        logger.error(e.__class__.__name__ + ": " + str(e))

