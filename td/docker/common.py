import docker
import td.logger.logger

logger = td.logger.logger.get_logger(__name__)

def get_running_containers():
    """Get running containers
    
    Returns:
        [List] -- List of running containers
    """

    try:
        client = docker.from_env()
        res = client.containers.list(filters={"label":"name=webapp"})

        return res

    except Exception as e:
        logger.error(e.__class__.__name__ + ": " + str(e))


def format_container_log_line(name, line):
    """Format container output log line
    
    Arguments:
        name {str} -- Container name
        line {str} -- Log line
    
    Returns:
        str -- formatted log line including container name
    """

    tokens = line.split(' ', 1)
    tokens.insert(1, name)
    return '{} [{}]: {}'.format(tokens[0], tokens[1], tokens[2].rstrip())
