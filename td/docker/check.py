import td.docker.common
import td.logger.logger
import requests

logger = td.logger.logger.get_logger(__name__)

def check(port, debug=False):
    """Checks running containers health
    
    Arguments:
        port {str} -- Container exposed port in port_number/protocol format (5000/TCP)
    
    Keyword Arguments:
        debug {bool} -- debug switch (default: {False})
    """

    output_format='{:<20} {:<8}'

    print(output_format.format('NAME','STATUS'))

    try:
        for c in td.docker.common.get_running_containers():
            r = requests.get("http://localhost:" + c.attrs['NetworkSettings']['Ports'][port][0]['HostPort'] + '/healthz')
            print(output_format.format(c.name, 'OK' if r.status_code == 200 else 'ERROR (' + str(r.status_code) + ')'))
    except Exception as e:
        logger.error(e.__class__.__name__ + ": " + str(e))



