import docker
import td.logger.logger

logger = td.logger.logger.get_logger(__name__)


def run(image, port, debug=False):
    """Starts a new container
    
    Arguments:
        image {str} -- The image name of the new container
        port {str} -- Container exposed port in port_number/protocol format (5000/TCP)
    
    Keyword Arguments:
        debug {bool} -- debug switch (default: {False})
    """

    client = docker.from_env()

    try:
        logger.info('Starting container based on ' + image + '...')
        res = client.containers.run(
            image,
            remove=True,
            detach=True,
            ports={port: None},
            labels={'name': 'webapp'}
        )
        logger.info(res.name + ' container started')
    except Exception as e:
        logger.error(e.__class__.__name__ + ": " + str(e))

