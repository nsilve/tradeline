import td
import td.logger.logger

logger = td.logger.logger.get_logger(__name__)


def stop(debug=False):
    """Stops all running containers
    
    Keyword Arguments:
        debug {bool} -- debug switch (default: {False})
    """

    try:
        for c in td.docker.common.get_running_containers():
            logger.info('Stopping container ' + c.name)
            c.stop()
    except Exception as e:
        logger.error(e.__class__.__name__ + ": " + str(e))
