import logging


def get_logger(name, level=logging.DEBUG):
    """Gets new logger
    
    Arguments:
        name {str} -- module name
    
    Keyword Arguments:
        level {integer} -- log level (default: {logging.DEBUG})
    
    Returns:
        RootLogger -- logger
    """

    logger = logging.getLogger(name)
    handler = logging.StreamHandler()
    formatter = logging.Formatter('%(asctime)s %(levelname)s [%(name)s]: %(message)s')
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    logger.setLevel(level)
    return logger
