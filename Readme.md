# Description
Tradeline Docker tool
The tool is designed to run as a Docker container

# Contents
`td`: Python package of the solution

`webapp`: Docker webapp image folder

# Usage
## Build
To build the Docker image:
```
make
```

## Run
To run the tool:
```
./td.sh
```

## Parameters
The tool supports various actions and verbose output switch

# Examples
```
➜  ⎈ dev1.loro tradeline git:(master) ✗ ./td.sh      
usage: td [-h] [-v] {build,run,check,monitor,log,logf,stop}

Tradeline Docker CLI

positional arguments:
  {build,run,check,monitor,log,logf,stop}
                        available actions

optional arguments:
  -h, --help            show this help message and exit
  -v, --debug           show verbose output
```

```
➜  ⎈ dev1.loro tradeline git:(master) ✗ ./td.sh build
2019-01-13 13:42:30,938 INFO [td.docker.build]: Image webapp:1.0.0 has been successfully built
```

```
➜  ⎈ dev1.loro tradeline git:(master) ✗ ./td.sh build -v
2019-01-13 13:42:56,115 DEBUG [td.docker.build]: Step 1/9 : FROM python:3.7-slim
...
2019-01-13 13:42:56,122 DEBUG [td.docker.build]: Step 9/9 : CMD ["app.py"]
2019-01-13 13:42:56,122 DEBUG [td.docker.build]: 
2019-01-13 13:42:56,123 DEBUG [td.docker.build]:  ---> Using cache
2019-01-13 13:42:56,123 DEBUG [td.docker.build]:  ---> 094ef954703d
2019-01-13 13:42:56,123 DEBUG [td.docker.build]: Successfully built 094ef954703d
2019-01-13 13:42:56,124 DEBUG [td.docker.build]: Successfully tagged webapp:1.0.0
2019-01-13 13:42:56,124 INFO [td.docker.build]: Image webapp:1.0.0 has been successfully built
```