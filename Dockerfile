FROM python:3.7-slim
MAINTAINER Silvestros Nikos "nsilvestros@gmail.com"
COPY . /app
WORKDIR /app
RUN pip install -r requirements.txt && \
    groupadd -r td && \
    useradd -r -g td -G root td
USER td
ENTRYPOINT ["python", "-m", "td"]
CMD ["-h"]
