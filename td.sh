#!/usr/bin/env bash

NAME=td
VERSION=1.0.0

docker run --rm -t -v /var/run/docker.sock:/var/run/docker.sock --network host ${NAME}:${VERSION} "$@"